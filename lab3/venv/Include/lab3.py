# Задание 1
import functions as fun
fun.hello_function()
fun.param_function("Hlistal", "Veaceslav");
fun.param_function2();

print(fun.sum_function(123, 456))

nmbr_list = [2, 4, 6, 8] #список
new_list = set(filter(lambda x:(x%2 == 1), nmbr_list))
print(new_list)

print("Enter your age please:")
yourAge = input()
if int(yourAge) > 65:
    print("Take care of your Health")
elif int(yourAge) > 18:
    print("We can sell you alchogol")
else:
    print("Wait a couple of years")


nmbr_list2 = (2, 4)
multiply = 1
for value in nmbr_list2:  #проход по кортежу
    multiply *= value
print("Result:", multiply)

nmbr_list3 = [2, 4, 6, 8]
division = 100
for v in nmbr_list3: #проход по списку
    division -= v
print("Result: ", division)

set1 = {1, 2, 3 , 4, 4, 5, 1}
for v1 in set1: #проход по кортежу
    v1 = v1 + 2
print("Result:", v1)

n = 10
sum = 0
i = 1
while i <= n:
    sum = sum + i
    i += 1
print("The sum is", sum)


#Задание 2
x = 0
while True:
    x += 1
    print(x)
    if x == 10:
        break

for char in "PYTHON STRING":
    if char == ' ':
        break
    print(char,)
    if char == 'O':
        continue


