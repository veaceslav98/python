def hello_function():
    print("Hello World")

def param_function(param1, param2):
    print("Hello Mr(s) ", param1, param2)

def param_function2(param1 = "Ivan", param2 = "Ivanov"):
    print("Hello Mr(s) ", param1, param2)

def sum_function(param1, param2):
    return param1 + param2