import datetime


class Employee:
    """Employee class which contains Employee name, his phone, date of birth, email and his position"""

    def __init__(self, nameEmployee="John", phone=212121, bday=datetime.datetime(1998, 5, 19), email="sdf@gmail.com",
                 position="worker"):
        self.__nameEmployee = nameEmployee
        self.__phone = phone
        self.__bday = bday
        self.__email = email
        self.__position = position

    @property
    def nameEmployee(self):
        return self.__nameEmployee

    @property
    def phone(self):
        return self.__phone

    @property
    def bday(self):
        return self.__bday

    @property
    def email(self):
        return self.__email

    @property
    def position(self):
        return self.__position

    @nameEmployee.setter
    def nameEmployee(self, nameEmployee):
        self.__nameEmployee = nameEmployee

    @phone.setter
    def phone(self, phone):
        self.__phone = phone

    @bday.setter
    def bday(self, dbay):
        self.__bday = dbay

    @email.setter
    def email(self, email):
        self.__email = email

    @position.setter
    def position(self, position):
        self.__position = position

    def calculateAge(self):
        now = datetime.datetime.now()
        age = now.year - self.__bday.year
        return age

    def _calculateSalary(self):
        pass
