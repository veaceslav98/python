from Employee import Employee


# В скобках указывем класс от которого наследуем имеющийся класс
class HourlyEmployee(Employee):
    def __init__(self, nmbrOfHour=5, hourlyPay=21):
        # вызов super() функции от родителького класса(в данном случае Employee)
        super().__init__()
        self.__nmbrOfHour = nmbrOfHour
        self.__hourlyPay = hourlyPay

    @property
    def nmbrOfHour(self):
        return self.nmbrOfHour

    @property
    def hourlyPay(self):
        return self.hourlyPay

    @nmbrOfHour.setter
    def nmbrOfHour(self, nmbrOfHour):
        self.__nmbrOfHour = nmbrOfHour

    @hourlyPay.setter
    def hourlyPay(self, hourlyPay):
        self.__hourlyPay = hourlyPay

    def calculateSalary(self):
        return self.__nmbrOfHour * self.__hourlyPay
