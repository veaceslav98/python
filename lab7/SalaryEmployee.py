from Employee import Employee


# В скобках указывем класс от которого наследуем имеющийся класс
class SalaryEmployee(Employee):
    def __init__(self, salary=2324):
        # вызов super() функции от родителького класса(в данном случае Employee)
        super().__init__()
        self.__salary = salary

    @property
    def salary(self):
        return self.salary

    @salary.setter
    def salary(self, salary):
        self.__salary = salary

    def calculateSalary(self):
        return self.__salary
