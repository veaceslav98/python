from Employee import Employee
from HourlyEmployee import HourlyEmployee
from SalaryEmployee import SalaryEmployee

emp = Employee()
print(emp.nameEmployee, emp.phone, emp.email, emp.bday, emp.position)

# Результат со значениями по умолчанию
hourlyEmployee = HourlyEmployee()
hourlySalary = hourlyEmployee.calculateSalary()
print(hourlySalary)

# Результат со значениями переданными в __init()__
hourlyEmployee2 = HourlyEmployee(100, 200)
hourlySalary2 = hourlyEmployee2.calculateSalary()
print(hourlySalary2)

# Результат со значениями по умолчанию
salaryEmployee = SalaryEmployee()
fixedSalary = salaryEmployee.calculateSalary()
print(fixedSalary)

# Результат со значениями переданными в __init()__
salaryEmployee2 = SalaryEmployee(123123123)
fixedSalary2 = salaryEmployee2.calculateSalary()
print(fixedSalary2)
