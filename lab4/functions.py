def addElements(elementList, element):
    elementList.append(element)


def deleteElements(elementList, element):
    try:
        elementList.remove(element)
        print("Element was removed")
    except ValueError:
        print("NoSuchElement")


def printListElement(elementList):
    print(elementList)


def menu():

    elementList = []

    while True:
        print("1.Press (1) to display current product in list")
        print("2.Press (2) to add product to list")
        print("3.Press (3) to remove product from list")
        print("4.Press (-1) to exit from menu")

        print("Enter menu option:")
        inputValue = int(input())

        if inputValue == 1:
            printListElement(elementList)
        elif inputValue == 2:
            print("Input element we should add:")
            deleteValue = input()
            addElements(elementList, deleteValue)
        elif inputValue == 3:
            print("Input element we should remove:")
            deleteValue = input()
            deleteElements(elementList, deleteValue)
        elif inputValue == -1:
            break
        else:
            print("Input valid value please!")


def second_task():
    print(max([1, 2, 3, 4, 5, 6, 7]))
    print(max([1, 2, 3, 4], [1, 2, 4, 5]))

    set1 = {1, 2, 3, 4, 5}
    set2 = {3, 4, 5, 6}

    print(set1.difference(set2))

    diction = {"key1": "value1", "key2": "value2", "key3": "value3"}
    print(diction.popitem())
    print(diction)


def main():
    menu()
    second_task()