from os.path import exists


def openFileForRead(inputFile):
    if exists(inputFile):
        myFileRead = open(inputFile, mode="r", encoding="utf-8")
        for num, line in enumerate(myFileRead, 1):
            print(str(num) + ":\t" + line.strip())

        myFileRead.close()
    else:
        print("NoSuchFileError")


def openFileForWrite(outPutFile, firstName, lastName, childrenNumber):
    if exists(outPutFile):
        myFileWrite = open(outPutFile, mode="w", encoding="utf-8")
        myFileWrite.write(firstName + "\t" + lastName + "\t" + childrenNumber + "\n")

        myFileWrite.close()
    else:
        print("NoSuchFileError")


def openFileForAppend(outPutFile, firstName, lastName, childrenNumber):
    if exists(outPutFile):
        myFileWrite = open(outPutFile, mode="a", encoding="utf-8")
        myFileWrite.write(firstName + "\t" + lastName + "\t" + childrenNumber + "\n")

        myFileWrite.close()
    else:
        print("NoSuchFileError")

