import re

import functions as fun

inputFile = "data.txt"


def dataValidate(firstName, lastName, childrenNumber):
    global checkedFirstName, checkedLastName, checkedChildrenNumber
    regularsExpr = r"[A-Za-z]+"
    regularsExpr2 = r"[0-9]{1,}"
    while True:
        for firstNameCheck in firstName:
            checkedFirstName = re.match(regularsExpr, firstNameCheck)
            if checkedFirstName is None:
                break

        for lastNameCheck in lastName:
            checkedLastName = re.match(regularsExpr, lastNameCheck)
            if checkedLastName is None:
                break

        for childrenNumberCheck in childrenNumber:
            checkedChildrenNumber = re.match(regularsExpr2, childrenNumberCheck)
            if checkedChildrenNumber is None:
                break

        if checkedFirstName and checkedLastName and checkedChildrenNumber:
            print("Element was added")
            break
        else:
            print("Try again")
            menu()


def menu():
    while True:
        print("\n***************MENU***************")
        print("1.Press (1) Append data in file")
        print("2.Press (2) View information in file")
        print("3.Press (3) Rewrite information in file")
        print("4.Press (-1) to exit from menu")
        print("**********************************\n")

        print("Enter menu option:")
        inputValue = int(input())

        if inputValue == 1:
            print("Input element in file")
            firstName = input("Your first name: ")
            lastName = input("Your last name: ")
            childrenNumber = input("Number of children in a family: ")

            dataValidate(firstName, lastName, childrenNumber)

            fun.openFileForAppend(inputFile, firstName, lastName, childrenNumber)
        elif inputValue == 2:
            fun.openFileForRead(inputFile)
        elif inputValue == 3:
            print("Rewrite element in file")
            firstName = input("Your first name: ")
            lastName = input("Your last name: ")
            childrenNumber = input("Number of children in a family: ")

            dataValidate(firstName, lastName, childrenNumber)

            fun.openFileForWrite(inputFile, firstName, lastName, childrenNumber)
        elif inputValue == -1:
            break
        else:
            print("Input valid value please!")


menu()
