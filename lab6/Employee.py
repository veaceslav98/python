import datetime


class Employee:
    """Employee class which contains Employee name, his phone, date of birth, email and his position"""

    # Конструктор с параметрами и значениями по умолчанию
    def __init__(self, nameEmployee="John", phone=212121, bday=datetime.datetime(1998, 5, 19), email="sdf@gmail.com",
                 position="worker"):
        self.__nameEmployee = nameEmployee
        self.__phone = phone
        self.__bday = bday
        self.__email = email
        self.__position = position

    # Getter-ы и Setter-ы для получения доступа к private полям. Без использования property()
    # def get_nameEmployee(self):
    #     return self.__nameEmployee
    #
    # def get_phone(self):
    #     return self.__phone
    #
    # def get_bday(self):
    #     return self.__bday
    #
    # def get_email(self):
    #     return self.__email
    #
    # def get_position(self):
    #     return self.__position
    #
    # def set_nameEmployee(self, nameEmployee):
    #     self.__nameEmployee = nameEmployee
    #
    # def set_phone(self, phone):
    #     self.__phone = phone
    #
    # def set_bday(self, dbay):
    #     self.__bday = dbay
    #
    # def set_email(self, email):
    #     self.__email = email
    #
    # # Пример использования pass. Заглушка, которая ставится на время пока не будет реализации метода
    # def set_position(self, position):
    #     pass
    #
    # # Использование property()
    # nameEmployee = property(get_nameEmployee, set_nameEmployee)
    # phone = property(get_phone, set_phone)
    # bday = property(get_bday, set_bday)
    # email = property(get_email, set_email)
    # position = property(get_position, set_position)

    # ---------------------Переписать с использованием декоратора--------------------------

    # Метод типа getter, украшен «@property», то есть помещаем эту строку непосредственно перед заголовком метода
    @property
    def nameEmployee(self):
        return self.__nameEmployee

    @property
    def phone(self):
        return self.__phone

    @property
    def bday(self):
        return self.__bday

    @property
    def email(self):
        return self.__email

    @property
    def position(self):
        return self.__position

    # Метод, который должен функционировать как setter, украшен "@name.setter" или "@color.setter
    @nameEmployee.setter
    def nameEmployee(self, nameEmployee):
        self.__nameEmployee = nameEmployee

    @phone.setter
    def phone(self, phone):
        self.__phone = phone

    @bday.setter
    def bday(self, dbay):
        self.__bday = dbay

    @email.setter
    def email(self, email):
        self.__email = email

    @position.setter
    def position(self, position):
        self.__position = position

    def calculateAge(self):
        now = datetime.datetime.now()
        age = now.year - self.__bday.year
        return age

    def _calculateSalary(self):
        salary = 3000
        return salary
