#Задание 1
list1 = [1, "22", 36.6, "Hello", "22"]
#print(list1)
#print("first element:", list1[0], "\nthird element:", list1[2]) #вывод первого и третьего значения списка
list1[3] = "World" #замаена четвертого элемента списка
#print(list1[3]) #вывод четвертого элемента списка

list2 = (1, "22", 36.6, "Hello") # инициализация кортежа
#print(type(list2)) # вывод типа
#print("first element:", list2[0] , "\nlast element:", list2[-1]) # вывод первого и последнего элемента

set1 = {2, 3, 0, 5, 2, 5} # инициализация множества
#print(type(set1))
#print("set=", set1) # вывод множества

dictString = {"f":"hello", "s":"world", "t":"!"}#словарь на основе строк
dictNumber = {1:"hello", 2:"world", 3:"!"}#словарь на основе строк
#print(type(dictString))
# print(dictString["s"], dictString["f"])
#print(type(dictNumber))
#print(dictNumber[1], dictNumber[2])

x = 1
y = "11"
#print(type(str(x)))
#print(int(y))
#print(set(list1))

#Задание 2
list3 = [1, 2, 3]
list4 = ["ONE", "TWO", "THREE"]
str1 = "Value of number {} is {}, and value of number {} is {}"
#print(str1.format(list4[0], list3[0], list4[1], list3[1]))

y = 9
#years = input("Введите Ваш возраст: ")
#print("Ваш возраст ", int(years), "следовательно через ", y, "лет Вам будет ", int(years)+y, "лет")

x = 'Hello world'
y = {1:'a',2:'b'}
print('H' in x)
print('h' in x)
print('H' not in x)
print('h' not in x)





